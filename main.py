# input 
# ["75.0,27.5,33.4,33.6,27.8,31.2"]

# output
# {"temp1": 75.0, "temp2": 27.5, "temp3": 33.4, "temp4": 33.6, "temp5": 27.8, "temp6": 31.2}

import time

def get_input():
    return ["75.0, 27.5, 33.4, 33.6, 27.8, 31.2"]


def get_dataframe(keys, values):
    return dict(zip(keys, values))


def main():
    data1 = get_input() # fake input
    data1 = (float(i) for i in data1[0].split(","))
    keys = ["Temperature" + str(i) for i in range(1, 6)] + ["Lux",] # temperature1 ~ 5, Lux
    d1 = dict(zip(keys, data1))
    d1['Time'] = time.strftime('%y-%m-%d %H:%M:%S', time.localtime())
    print(d1)

main()
